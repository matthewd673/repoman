# repoman

A simple git repository manager

## Usage

*All repositories will be created in ./githost*

`./repoman create <repository name>`

`./repoman remove <repository name>`

`./repoman list`